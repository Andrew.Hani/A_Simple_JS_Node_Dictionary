var fs = require('fs');
var func = process.argv.slice(2)[0] || 'list';
var param = process.argv.slice(3);

function storeModule() {
  var _this = this;
  this.pairStore = {};

  function latest() {
    _this.pairStore = JSON.parse(fs.readFileSync('dictionary.json', 'utf8')); // Sync
  }
  this.add = function (myKey, myValue) {
    if (myKey && myValue) {
      this.pairStore[myKey] = myValue;
      console.log(this.pairStore);
      fs.writeFile('dictionary.json', JSON.stringify(this.pairStore), function (err) {
        if (err) throw err;
        console.log('Updated!');
      });
    }else if(myKey){
      console.log('Please add a value to your key!');
    }else{
      console.log('You have to enter a key and a value pair in order to accomplish the function.');
    }
  }
  this.list = function () {
    latest();
    console.log(this.pairStore);
    return this.pairStore;
  }
  this.get = function (myKey) {
    latest();
    if (this.pairStore.hasOwnProperty(myKey)) {
      console.log(this.pairStore[myKey]);
      return this.pairStore[myKey];
    } else {
      return 'Sorry the sotre can\'t identify this key';
    }
  }
  this.remove = function (myKey) {
    latest();
    if (this.pairStore.hasOwnProperty(myKey)) {
      delete this.pairStore[myKey];
      fs.writeFile('dictionary.json', JSON.stringify(this.pairStore), function (err) {
        if (err) throw err;
        console.log(myKey + ' is deleted successfully from your dictionary!');
      });
      return this.pairStore;
    } else {
      console.log('Sorry the sotre can\'t identify this key');
    }
  }
  this.clear = function () {
    this.pairStore = {};
    fs.writeFile('dictionary.json', JSON.stringify(this.pairStore), function (err) {
      if (err) throw err;
      console.log('Your store is successfully cleared');
    });
    return 'Your store is successfully cleared';
  }
};
var store = new storeModule();
if(typeof store[func] == 'function') store[func](param[0],param[1]);
else console.log('"'+func+'" is not a function');